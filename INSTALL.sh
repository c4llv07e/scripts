#!/usr/bin/bash

output_dir=${XDG_BIN_HOME:-~/.local/bin}

cd $(dirname -- ${0})
for scr in $(find . -perm -u+x -type f); do
  bs=$(basename -- ${scr})
  [[ "$(basename -- ${0})" = "${bs}" ]] && continue
  [[ ! -e ${output_dir}/${bs} ]] && \
    chmod +x ${bs} && \
    ln -s $(pwd)/${bs} ${output_dir}/ && \
    echo "${bs} (${scr}) was added"
done
